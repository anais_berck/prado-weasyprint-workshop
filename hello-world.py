from weasyprint import HTML

# Let weasyprint deal with reading the files and give the filenames
HTML(filename='hello-world.html').write_pdf('hello-world.pdf')
